package it.nodereader.model;

import java.util.List;

public class FrontendListResponse <T> {

	private List<T> data ;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
