package it.nodereader.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.nodereader.entity.NodeEntity;

public interface NodeRepository extends JpaRepository<NodeEntity, Long> {
	
	Optional<List<NodeEntity>> findByParentId(long id);
	
}
